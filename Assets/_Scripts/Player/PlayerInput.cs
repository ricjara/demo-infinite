﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Ensure all referenced components are in the GameObject
[RequireComponent(typeof(FlyMovement))]
public class PlayerInput : MonoBehaviour {

	FlyMovement movement;

	void Start () {
		// Initialize components
		movement = GetComponent<FlyMovement>();
	}
	
	// FixedUpdate is called before every physics calculation cycle
	// ---- We call FixedUpdate instead of Update because we use physics-based movement
	void FixedUpdate () {
		// Get axes related input (defined in project input configuration)
		// ---- Input.GetRawAxis (and GetAxis) returns 0 > value >= 1 for right/up
		// ---- Input.GetRawAxis (and GetAxis) returns -1 <= value < 0 for left/down
		// ---- Input.GetRawAxis (and GetAxis) returns 0 for none
		// Vector2 moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		// In this case we only want to move vertically so we use the vertical axis
		Vector2 moveInput = new Vector2(0f, Input.GetAxisRaw("Vertical"));
		// Move the character
		movement.Move(moveInput);
	}
}

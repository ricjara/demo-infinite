﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Ensure required components are in the GameObject
[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour {

	[Range(1,1000)]
	public int maxHealth = 5;
	
	// We want to show the health, but not edit it
	public int currentHealth;
	Animator anim;

	void Start () {
		// Initialize components
		anim = GetComponent<Animator>();
		currentHealth = maxHealth;
	}

	// Damages the player
	public void TakeDamage (int dmg) {
		currentHealth = Mathf.Clamp(currentHealth-dmg, 0, maxHealth);
		
		// If the health reaches 0, die
		if(currentHealth == 0) {
			Die();
		}
	}

	// Hnadles the player dying
	void Die () {
		// Demo purposes, throw message in console
		Debug.Log("lol rekt");
	}
}
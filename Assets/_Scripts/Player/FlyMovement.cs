﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// To ensure all referenced components are in the GameObject
[RequireComponent(typeof(Rigidbody2D))]
public class FlyMovement : MonoBehaviour {

	// We want to have a slider in the inspector and limit the maxSpeed up to 5.0f
	[Range(0,5f)]
	public float maxSpeed = 3.5f;

	Rigidbody2D rb;

	// Unity event function -> Runs when component initializes
	void Start () {
		// Initialize the components attached to the GameObject
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Control the movement logic
	public void Move (Vector2 velocity) {
		 // Normalize the velocity vector and apply speed
		 rb.velocity = velocity.normalized * maxSpeed;
	}
}

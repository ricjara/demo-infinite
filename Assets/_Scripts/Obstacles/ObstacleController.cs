﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class ObstacleController : MonoBehaviour {
	
	[Range(0, 1000)]
	public int damage = 10;

	// Detect collisions with other objects
	void OnCollisionEnter2D(Collision2D other) {
		// If the object is the player, damage them and destroy the obstacle
		if (other.gameObject.tag.Equals("Player")) {
			PlayerController pc = other.gameObject.GetComponent<PlayerController>();
			pc.TakeDamage(damage);
			Destroy(gameObject);
		}
	}
}

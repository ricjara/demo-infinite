﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Ensure the required components are present in the GameObject
[RequireComponent(typeof(Rigidbody2D))]
public class ObstacleMovement : MonoBehaviour {

	[Range(-10f,10f)]
	public float xVelocity = -1.5f;

	Rigidbody2D rb;

	void Start () {
		// Get rigidbody and set starting velocity
		rb = GetComponent<Rigidbody2D>();
		rb.velocity = new Vector2(xVelocity, 0);
	}
}

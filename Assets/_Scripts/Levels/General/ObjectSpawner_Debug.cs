﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// We want to debug this during edit mode (Not only on play)
[ExecuteInEditMode]
// Ensure required components are in the GameObject
[RequireComponent(typeof(ObjectSpawner))]
public class ObjectSpawner_Debug : MonoBehaviour {

	ObjectSpawner os;

	void Start () {
		os = GetComponent<ObjectSpawner>();
	}
	
	// Update is called once per frame
	void Update () {
		// Draw limits to the spawner every frame during editing
		os.DrawLimits();
	}
}

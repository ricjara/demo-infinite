﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour {

	// Destroys everything that enters the trigger collider
	void OnTriggerEnter2D (Collider2D other) {
		Destroy(other.gameObject);
	}
}

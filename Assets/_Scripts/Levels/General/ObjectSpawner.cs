﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour {

	// Range (in gameUnits) to spawn objects
	[Range(1f, 25f)]
	public float verticalRange = 4f;

	// For the demo we use the spawner for only one type of object
	public GameObject spawnedObject;

	// Use this for initialization
	void Start () {
		// Spawn the first object as soon as this script initializes
		Invoke("SpawnObject", 0f);
	}

	// Used to draw the limits of the spawner according to vertical Range (Debug Purposes)
	public void DrawLimits () {
		Vector2 originTop = new Vector2(transform.position.x, transform.position.y + verticalRange/2);
		Vector2 originBottom = new Vector2(transform.position.x, transform.position.y - verticalRange/2);
		float rayLength = 5f;
		// We assume the player goes from left to right, so the spawner points to the left
		Debug.DrawRay(originTop, Vector2.left*rayLength, Color.red);
		Debug.DrawRay(originBottom, Vector2.left*rayLength, Color.red);
	}

	void SpawnObject () {
		// Set position in the Y axis to spawn inside the allowed range
		float yPosition = transform.position.y + Random.Range(-verticalRange/2f,verticalRange/2f);
		Vector3 spawnPosition = new Vector3(transform.position.x, yPosition, transform.position.z);
		// We create a new instance of the object
		GameObject o = Instantiate(spawnedObject, spawnPosition, transform.rotation);
		// We assign the instance as a child of the spawner
		o.transform.parent = transform;

		// Recursively call the spawn function according to difficulty algorithm
		// ---- No difficulty algorithms for this demo, just a random range
		float spawnInterval = Random.Range(1f, 2.5f);
		Invoke("SpawnObject", spawnInterval);
	}
}
